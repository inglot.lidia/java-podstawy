package sample;

public class Czlowiek {


    String imie;
    String nazwisko;
    int wzrost;
    short wiek;

    void setImie(String imie) {

        this.imie = imie;
    }

    void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    void setWzrost(int wzrost) {
        this.wzrost = wzrost;
    }

    void setWiek(short wiek) {
        this.wiek = wiek;
    }


    String getImie() {
        return this.imie;
    }

    int getWzrost() {
        return this.wzrost;
    }
}
